import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

//teht 3
public class Teht3 {

	interface Arpoja {
		void kaynnista();
	}

	public static void main(String args[]) {
		// Tapa 1
		List<Integer> numerot = IntStream.generate(() -> {
			return (int) (Math.random() * 40) + 1;
		}).distinct() // not duplicates
				.limit(7).boxed().collect(Collectors.toList());

		System.out.println(numerot);
		// Tapa 2
		Arpoja arpoja = new Arpoja() {
			@Override
			public void kaynnista() {
				// printing age
				List<Integer> numerot = IntStream.generate(() -> {
					return (int) (Math.random() * 40) + 1;
				}).distinct() // not duplicates
						.limit(7).boxed().collect(Collectors.toList());
				System.out.println(numerot);
			}
		};
		arpoja.kaynnista();

	}
}