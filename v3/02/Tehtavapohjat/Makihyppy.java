import java.util.function.DoubleUnaryOperator;
// Tehtava 2
public class Makihyppy {

	static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet) {
		DoubleUnaryOperator dl = (double pituus) -> {
			double pisteet = 60;
			if (pituus > kPiste) {
				pisteet += (pituus - kPiste) * lisapisteet;
			} else if (pituus < kPiste) {
				pisteet += (kPiste - pituus) * -lisapisteet;
			}
			return pisteet;
		};
		return dl;
	}

	public static void main(String[] args) {

		DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 1.8);

		System.out.println(normaaliLahti.applyAsDouble(100));

	}

}