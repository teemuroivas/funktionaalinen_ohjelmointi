import java.util.function.Supplier;

class Teht1{
    
    public static <T> void main(String[] args){
    	
    	Tulostaja2 t = new Tulostaja2();
        
    	Supplier<Integer> generaattori = () -> 2;  
    	Supplier<Integer> generaattori2 = () -> (int)(Math.random() * 6 + 1);  

    	t.tulosta(generaattori);
    	t.tulosta(generaattori2);
        t.tulosta(() -> 100);
    }   
    
}


