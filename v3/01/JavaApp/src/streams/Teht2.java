package streams;

import java.util.*;
import static menu.Dish.menu;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class Teht2 {
	public static void main(String args[]) {
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario", "Milan");
		Trader alan = new Trader("Alan", "Cambridge");
		Trader brian = new Trader("Brian", "Cambridge");

		List<Transaction> transactions = Arrays.asList(new Transaction(brian, 2011, 300),
				new Transaction(raoul, 2012, 1000), new Transaction(raoul, 2011, 400),
				new Transaction(mario, 2012, 710), new Transaction(mario, 2012, 700), new Transaction(alan, 2012, 950));

		// tehtava 2
		List<Transaction> after_2012 = transactions.stream()
				.filter(transaction -> transaction.getYear() >= 2012 && transaction.getValue() >= 900)
				.sorted(comparing(t -> t.getValue())).collect(toList());
		System.out.println(after_2012);

		Integer dishCount = menu.stream().map(dish -> 1).reduce(0, (a, b) -> a + b);

		System.out.println(dishCount);
	}
}