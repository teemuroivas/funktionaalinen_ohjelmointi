package streams;
import java.util.*;

public class Teht1 {
	
	interface Calculator {
		double calculate(double n); 
	}

	public static void main(String args[]) {
		Calculator toCelsius = (n) -> (5.0/9.0)*(n-32.0);
		Calculator area = (n) -> Math.PI * n * n;

		System.out.println(toCelsius.calculate(100)); // 100 fahrenheits = ~37 celsius
		
		System.out.println(area.calculate(10)); // ~314

	}
}