package streams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Teht4 {

	public static void main(String args[]) {
		List<Integer> luvut1 = Arrays.asList(1, 2, 3);
		List<Integer> luvut2 = Arrays.asList(3, 4);

		List<String> kombinaatiot = luvut1.stream().flatMap(
				str1 -> luvut2.stream().map(str2 -> "(" + String.valueOf(str1) + "," + String.valueOf(str2) + ")"))
				.collect(Collectors.toList());

		System.out.println(kombinaatiot);

	}
}