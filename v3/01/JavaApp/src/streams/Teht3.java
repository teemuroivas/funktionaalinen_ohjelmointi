package streams;

import static java.util.stream.Collectors.toList;
import java.util.*;
import java.util.stream.Stream;

public class Teht3 {

	public static void main(String args[]) {
		int kutosienMaara = Stream.generate(() -> (int) (Math.random() * 6) + 1).limit(20)
				.filter(luku -> luku == 6).collect(toList()).size();

		System.out.println(kutosienMaara);
	}
}