(ns test-lein.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
; tehtävä 4
(defn square [luku] (* luku luku))
; tehtävä 5
(defn karkausvuosi?
  [year]
  (cond (zero? (mod year 400)) true
        (zero? (mod year 100)) false
        (zero? (mod year 4)) true
        :default false))
