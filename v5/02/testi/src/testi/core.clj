(ns testi.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

;(use 'testi.core :reload)
; Tehtävä 1
(defn parillinen? []
    (println "Anna luku")
    (let [luku (Integer. (read-line))]
    (if (> luku 0)
        (if (zero? (mod luku 2)) "luku on parillinen" "luku on pariton")
        (println "Luku ei voi olla 0 eikä negatiivinen.")
        )))
  
;Tehtävä 2
(defn parillinen2? []
    (println "Anna luku")
    (let [luku (Integer. (read-line))]
    (if (> luku 0)
        (if (zero? (mod luku 2)) "luku on parillinen" "luku on pariton")
        (do
            (println "Luku ei voi olla 0 eikä negatiivinen.")
            (recur)
        ))))
    
;Tehtävä 3
(defn jaollinen3? [luku]
    (loop [kierros 1]
        (when (<= kierros luku)
        (if (zero? (mod kierros 3))
            (println kierros))
        (recur(+ kierros 1))
        )
    )
)

;Tehtävä 4
(defn lotto []
    (def setti #{})
    (loop [kierros 1]
        (when (< (count setti) 7)
            (def luku (+ (rand-int 39) 1))
            (def setti (conj setti luku))
            (recur(inc kierros))
        )
    )
    (println setti)
)

;Tehtävä 5
(defn syt [p, q]
    (if (= q 0)
        p
        (syt q (mod p q))
    )
)


