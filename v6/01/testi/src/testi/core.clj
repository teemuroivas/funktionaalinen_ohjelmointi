(ns testi.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(def lampo15 [-5 -3 -1 2 7 12 13 15 9 4 0 -3])
(def lampo16 [-6 -4 0 2 7 15 12 12 8 4 0 -5])

 (def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])

(def sum #(reduce + %))
(def avg #(/ (sum %) (count %)))

;Tehtävä 1
;kutsu (positiivinenka) oikea vast: 8.71....
(defn positiivinenka []
  (->> (map + lampo15 lampo16)
  (map #(/ % 2))
  (filter #(< 0 %))
  (avg)
  (double)
  )
)

;Tehtävä2
;kutsu (huhtineste)
(defn huhtineste []
  (->> (filter #(= (:kk %) 4) food-journal)
       (map #(- (:neste %) (:vesi %)))
       (sum)
  )
)




