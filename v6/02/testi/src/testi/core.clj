(ns testi.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn nimi [name] (str "Ystävällisin terveisin " name))
(def allekirjoitus (partial nimi))

(def vampire-database
  {0 {:makes-blood-puns? false, :has-pulse? true  :name "McFishwich"}
   1 {:makes-blood-puns? false, :has-pulse? true  :name "McMackson"}
   2 {:makes-blood-puns? true,  :has-pulse? false :name "Damon Salvatore"}
   3 {:makes-blood-puns? true,  :has-pulse? true  :name "Mickey Mouse"}})

(defn get-next []
  (+ (apply max (map #(first %) vampire-database)) 1)
) 

(def vectors
  [[1 2 3][4 5 6][7 8 9]]
)

;teht2
; (a
; (minimum vectors) ==> (1 4 7)
(defn minimum [v] 
  (map #(apply min %) v)
)

; (b
(apply vector (minimum vectors))
    
;teht 3

(defn lisaa-vampyyrikantaan 
    [mbp hp nimi]
    (def vampire-database (assoc vampire-database (get-next) {:makes-blood-puns? mbp, :has-pulse? hp  :name nimi}))
    ;; 
)

;teht 4

(defn poista-vampyyrikannasta
    [avain]
    (def vampire-database (dissoc vampire-database avain))
    ;; 
)

