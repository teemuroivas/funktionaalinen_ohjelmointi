'use strict'

const f = function () {
    return function (x) {
		return x+1;
	 }
	}(); // Huomaa funktion kutsu!

let tulos = f(3);

console.log(tulos);

console.log("viikko 1, 02");
const vertaileLukuja = function() {
    return function (luku1, luku2) {
		if (luku1 > luku2) {
			return 1;
		} else if (luku1 < luku2) {
			return -1;
		} else {
			return 0;
		}
	 }
	}();
	
console.log("teht 1");
console.log("1 = ensimmäinen luku on suurempi, -1 = toinen luku on suurempi, 0 = luvut ovat yhtä suuret");
tulos = vertaileLukuja(3,1);
console.log("3,1 =",tulos);
tulos = vertaileLukuja(1,5);
console.log("1,5 =",tulos);
tulos = vertaileLukuja(11,11);
console.log("11,11 =",tulos);

const lampotilat15 = [-5, -6, -2, 3, 10, 13, 16, 15, 10, 5, 0, -3]
const lampotilat16 = [-4, -6, -1, 5, 11, 13, 16, 14, 8, 5, 1, -6]

function saaVertailu(vertaileLukuja, lampotilat15, lampotilat16) {
	let tulosLista = []
	if (lampotilat15.length != lampotilat16.length) {
		return "Listan alkioiden lukumäärä ei täsmää." 
	}
	for (let i = 0; i < lampotilat15.length; i++) { 
    	tulosLista.push(vertaileLukuja(lampotilat15[i], lampotilat16[i]));
	}
	return tulosLista;
}

console.log("teht 2");
tulos = saaVertailu(vertaileLukuja, lampotilat15, lampotilat16)
console.log(tulos)
var counts = {};
tulos.forEach(function(x) { counts[x] = (counts[x] || 0)+1; });
console.log(counts);

