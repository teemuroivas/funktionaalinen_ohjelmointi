function fact(n) {
// triviaalitapaus
  if (n === 0) {
    return 1;
  }
 // perussilmukka
  return n * fact(n - 1);
}
var tulos = fact(4);
console.log(tulos);

function onPalindromi(merkkijono) {
  if (merkkijono.length < 2) {
    return true;
  } else if (merkkijono.charAt(0) != merkkijono.charAt(merkkijono.length-1)) {
  		return false;
  } else {
    return onPalindromi(merkkijono.substr(1, merkkijono.length-2))
  }
}
console.log("teht 1");
console.log('onko seppo palindromi?', onPalindromi('seppo'));
console.log('onko imaami palindromi?', onPalindromi('imaami'));

function syt(p, q) {
  if (q == 0) {
    return p;
  } else {
    return syt(q, p % q);
  }
}
console.log("teht 2");
console.log("102 & 68, yhteinentekijä on", syt(102, 68));
console.log("150 & 15, yhteinentekijä on", syt(155, 15));

function kjl(p, q) {
  if (q == 1) {
    return true;
  } else if (q == 0) {
    return false;
  } else {
    return kjl(q, p % q);
  }
}
console.log("teht 3");
console.log("Ovatko luvut 20 & 5 jaottomia keskenään?", kjl(20, 5));
console.log("Ovatko luvut 27 & 4 jaottomia keskenään?", kjl(27, 4));

function power(base, exponent) {
  if (exponent == 0)
    return 1;
  else
    return base * power(base, exponent - 1);
}

console.log("teht 4");
console.log("2^5 =", power(2,5));

function reverseArray(lista){
  var kaannetty = [];

  function kaantaja (lista){
    if (lista.length !== 0){
      kaannetty.push( lista.pop() );
      kaantaja(lista);
    }
  }

  kaantaja(lista);
  return kaannetty;
}
console.log("teht 5");
console.log("Lista [0,1,2,3,4,5,6,7,8,9] kaannetaan", reverseArray([0,1,2,3,4,5,6,7,8,9]));




