package parallel;

import java.util.stream.IntStream;

public class Teht2 {
	static int filterCount = 0, mapCount = 0;

	public static void main(String[] args) {
		// sum of the triples of even integers from 2 to 10
		System.out.printf("Sum of the triples of even integers from 2 to 10 is: %d%n",
				IntStream.rangeClosed(1, 10).filter(x -> {
					filterCount++;
					return x % 2 == 0;
				}).map(x -> {
					mapCount++;
					return x * 3;
				}).sum());

		System.out.println("Filter count: " + filterCount + " , Map count: " + mapCount);
		// Filter 10 kertaa, Map 5 kertaa
		filterCount = 0;
		mapCount = 0;

		System.out.printf("Map ja filter funktiot vaihtavat paikkoja, summa: %d%n",
				IntStream.rangeClosed(1, 10).map(x -> {
					mapCount++;
					return x * 3;
				}).filter(x -> {
					filterCount++;
					return x % 2 == 0;
				}).sum());

		System.out.println("Filter count: " + filterCount + " , Map count: " + mapCount);
		// Map 10 kertaa, Filter 10 kertaa

	}
}