package parallel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.stream.IntStream;

public class Teht3 {

	public static void main(String[] args) {
		ArrayList<Integer> taulukkoLista = new ArrayList<Integer>();
		LinkedList<Integer> linkitettyLista = new LinkedList<Integer>();
		long start, duration;
		int sum;
		
		for (int i = 1; i <= 1000; i++)
		{
		   taulukkoLista.add(i);
		   linkitettyLista.add(i);
		}
		IntStream stream = IntStream.range(1,1000);
		
		//LinkedList
		start = System.nanoTime();
		sum = linkitettyLista.parallelStream().map(x -> x * x).reduce(0, (acc, x) -> acc + x);
		duration = (System.nanoTime() - start) / 1_000_000;
		System.out.println("Linkedlist duration: " + duration + "msecs.");
		System.out.println("Sum: " + sum);
		
		//ArrayList
		start = System.nanoTime();
		sum = taulukkoLista.parallelStream().map(x -> x * x).reduce(0, (acc, x) -> acc + x);
		duration = (System.nanoTime() - start) / 1_000_000;
		System.out.println("Arraylist duration: " + duration + "msecs.");
		System.out.println("Sum: " + sum);
		
		//IntStream
		start = System.nanoTime();
		sum = stream.map(x -> x * x).reduce(0, (acc, x) -> acc + x);
		duration = (System.nanoTime() - start) / 1_000_000;
		System.out.println("Stream duration: " + duration + "msecs.");
		System.out.println("Sum: " + sum);
	}
}