package testing;

import static org.junit.Assert.assertEquals;

import java.util.*;
import java.util.stream.Collectors;

import org.junit.Test;
//Tehtävä 1
public class Debugging {

	public static List<Point> moveAllPointsRightBy(List<Point> points, int i) {
		return points.stream().map(p -> p.moveRightBy(i)).collect(Collectors.toList());

	}

	private static class Point {
		private int x;
		private int y;

		private Point(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public Point moveRightBy(int x) {
			return new Point(this.x + x, this.y);
		}
		
        @Override
        public boolean equals(Object object) {
        	if(object != null && object instanceof Point){
        		Point p = (Point) object;
        		if(p.x == this.x && p.y == this.y)
        			return true;
        	}
        	return false;
        }


	}

	@Test
	public void testMoveAllPointsRightBy() {
		try {
			List<Point> points = Arrays.asList(new Point(5, 5), new Point(10, 5));
			List<Point> expectedPoints = Arrays.asList(new Point(15, 5), new Point(20, 5));
			List<Point> newPoints = moveAllPointsRightBy(points, 10);
			assertEquals(expectedPoints, newPoints);
		} catch (Error e) {
			System.out.println(e);
		}
	}

}
