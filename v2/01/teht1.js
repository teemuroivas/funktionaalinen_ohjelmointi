var toCelsius = (fahrenheit) => {
    return (5/9) * (fahrenheit-32);
}
var area = (radius) => {  
        return Math.PI * radius * radius;  
};

console.log(toCelsius(100));
console.log(area(10));