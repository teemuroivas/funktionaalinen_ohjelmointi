const HEL_temp_15 = [-5, -6, -2, 3, 10, 13, 16, 15, 10, 5, 0, -3];
const HEL_temp_16 = [-4, -6, -1, 5, 11, 13, 16, 14, 8, 5, 1, -6];

let result = HEL_temp_15
                .map((x, i) => (x + HEL_temp_16[i])/2)
                .filter(x => x > 0)
                .reduce((previous, current, index, array) => {
                    previous += current
                    if (array.length-1 == index) {
                      previous = previous / array.length
                    }
                    return previous
                });


console.log("result: ", result);
