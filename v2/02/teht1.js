function laskepisteet(pituus, kPiste, lisapisteet) {
    let pisteet = 60;
    
    if (pituus > kPiste) {
        pisteet += (pituus-kPiste)*lisapisteet;
    } else if (pituus < kPiste) {
        pisteet += (kPiste-pituus)*-lisapisteet;
    }
    return pisteet;
}

console.log(laskepisteet(80,70,2));
console.log(laskepisteet(60,70,2));

function lp(kPiste, lisapisteet) {
    return function (pituus) {
        let pisteet = 60;
    
        if (pituus > kPiste) {
            pisteet += (pituus-kPiste)*lisapisteet;
        } else if (pituus < kPiste) {
            pisteet += (kPiste-pituus)*-lisapisteet;
        }
        return pisteet;
    }
}

console.log(lp(70, 2)(80));
console.log(lp(70,2)(60));

