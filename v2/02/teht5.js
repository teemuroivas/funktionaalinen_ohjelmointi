const Immutable = require('immutable');

var mappedSequence = Immutable.Seq([1,2,3,4,5,6,7,8]).map(x => x * x);
console.log(mappedSequence.last());