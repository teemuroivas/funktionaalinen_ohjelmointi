const Immutable = require('immutable');

const autoObject = Immutable.Record({
    matkam: 0,
    tankki: 0
});

class Auto extends autoObject {
  getTankki() {
    return this.tankki;
  }
  
  getMatkam() {
    return this.matkam;
  }
  
  tankkaa(maara) {
      this.tankki += maara;
  }
}

const auto = new Auto({matkam: 0, tankki: 80})

console.log("Tankki: " + auto.getTankki() + ", matkamittari: " + auto.getMatkam());
try {
    auto.matkam = 5; // throws Error
}
catch(err) {
    console.log(err);
}

try {
    auto.tankkaa(5);
}
catch(err) {
    console.log(err);
}

console.log("Tankki: " + auto.getTankki() + ", matkamittari: " + auto.getMatkam());
const auto2 = auto.set('matkam', 20);
console.log("Auto2, tankki: " + auto2.getTankki() + ", matkamittari: " + auto2.getMatkam());
