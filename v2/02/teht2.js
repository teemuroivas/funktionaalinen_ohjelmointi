const Auto = (function(){
    const suojatut = new WeakMap();
    
    class Auto {
      constructor(tankki, matkam) {
        suojatut.set(this, {matkam: matkam});
        this.tankki = tankki;
      }
      
        getMatkamittari() {
            return suojatut.get(this).matkam;
        }
        
        getTankki() {
            return this.tankki;
        }
        
        aja(määrä) {
            this.tankki -= määrä;
            suojatut.set(this, {matkam: this.getMatkamittari() + määrä});
        }
        
        tankkaa(määrä) {
            this.tankki += määrä;
        }
    }
    
    return Auto;
})();

const auto = new Auto(80,0);

console.log("Matkamittari: " + auto.getMatkamittari() + ", tankki: " + auto.getTankki());
console.log("Aja 10 eteenpäin (Matkamittari +10 ja tankki -10).");
auto.aja(10);
console.log("Matkamittari: " + auto.getMatkamittari() + ", tankki: " + auto.getTankki());

auto.tankki = 60;
auto.matkam = 20;
console.log("Yritetään muuttaa tankin arvo => 60 ja matkamittarin arvo => 20. Vain tankin arvo kuitenkin muuttuu, koska matkamittari on suojattu.");
console.log("Matkamittari: " + auto.getMatkamittari() + ", tankki: " + auto.getTankki());

auto.tankkaa(20); // +20 tankkiin
console.log("Tankataan 20 lisää.");
console.log("Matkamittari: " + auto.getMatkamittari() + ", tankki: " + auto.getTankki());



